#!/bin/bash
set -e

if [ -z "$USER" ];then
    export USER="$(id -un)"
fi
export LC_ALL=C
export GAPPS_SOURCES_PATH=vendor/opengapps/sources/

## set defaults

rom_fp="$(date +%y%m%d)"

myname="$(basename "$0")"
if [[ $(uname -s) = "Darwin" ]];then
    jobs=$(sysctl -n hw.ncpu)
elif [[ $(uname -s) = "Linux" ]];then
    jobs=$(nproc)
fi

romname="$1"

if [[ $1 == *evolutionx* || $1 == *pixelexperience* || $1 == *extendedui* ]]; then
if [[ $2 == *gapps* ]]; then
echo "Your build would not start if I let you implement gapps this way on this rom, please use vanilla variant or you will have to rethink your life choices."
exit 1
elif [[ $2 == *arm64* ]]; then
export TARGET_GAPPS_ARCH=arm64
echo The ROM you are going to build is $1
echo Your GApps architecture has been set to $TARGET_GAPPS_ARCH
elif [[  $2 != *arm64* ]]; then
export TARGET_GAPPS_ARCH=arm
echo The ROM you are going to build is $1
echo Your GApps architecture has been set to $TARGET_GAPPS_ARCH
fi
fi

if [[ $2 == *vanilla* ]]; then
export nogapps=true
fi

## handle command line arguments
read -p "Do you want to sync? (y/N) " choice

function help() {
    cat <<EOF
Syntax:

  $myname [-j 2] <rom type> <variant>...

Options:

  -j   number of parallel make workers (defaults to $jobs)

ROM types:

  havoc
  evolutionx
  extendedui

Variants are dash-joined combinations of (in order):
* processor type
  * "arm" for ARM 32 bit
  * "arm64" for ARM 64 bit
  * "a64" for ARM 32 bit system with 64 bit binder
* A or A/B partition layout ("aonly" or "ab")
* GApps selection
  * "vanilla" to not include GApps
  * "gapps" to include opengapps
  * "go" to include gapps go
  * "floss" to include floss
* SU selection ("su" or "nosu")
* Build variant selection (optional)
  * "eng" for eng build
  * "user" for prod build
  * "userdebug" for debug build (default)

for example:

* arm-aonly-vanilla-nosu-user
* arm64-ab-gapps-su
* a64-aonly-go-nosu
EOF
}

function get_rom_type() {
    while [[ $# -gt 0 ]]; do
        case "$1" in
	    havoc)
	    mainrepo="https://github.com/Havoc-OS/android_manifest.git"
		mainbranch="ten"
		localManifestBranch="android-10.0"
		treble_generate="havoc"
		extra_make_options="WITHOUT_CHECK_API=true"
		jack_enabled="false"
                ;;
	    extendedui)
	    mainrepo="https://github.com/Extended-UI/android_manifest.git"
		mainbranch="android_10"
		localManifestBranch="android-10.0"
		treble_generate="aosp"
		extra_make_options="WITHOUT_CHECK_API=true"
		jack_enabled="false"
                ;;
	    pixelexperience)
	    mainrepo="https://github.com/PixelExperience/manifest.git"
		mainbranch="ten"
		localManifestBranch="android-10.0"
		treble_generate="aosp"
		extra_make_options="WITHOUT_CHECK_API=true"
		jack_enabled="false"
                ;;
	   evolutionx)
	   	mainrepo="https://github.com/Evolution-X/manifest.git"
		mainbranch="ten"
		localManifestBranch="android-10.0"
		treble_generate="aosp"
		extra_make_options="WITHOUT_CHECK_API=true"
		jack_enabled="false"
                ;;
	    potato)
	    mainrepo="https://github.com/PotatoProject/manifest.git"
		mainbranch="croquette-release"
		localManifestBranch="android-10.0"
		treble_generate="potato"
		extra_make_options="WITHOUT_CHECK_API=true"
		jack_enabled="false"
	esac
        shift
    done
}

function parse_options() {
    while [[ $# -gt 0 ]]; do
        case "$1" in
            -j)
                jobs="$2";
                shift;
                ;;
        esac
        shift
    done
}

declare -A partition_layout_map
partition_layout_map[aonly]=a
partition_layout_map[ab]=b

declare -A gapps_selection_map
gapps_selection_map[vanilla]=v
gapps_selection_map[gapps]=g
gapps_selection_map[go]=o
gapps_selection_map[floss]=f

declare -A su_selection_map
su_selection_map[su]=S
su_selection_map[nosu]=N

function parse_variant() {
    local -a pieces
    IFS=- pieces=( $1 )

    local processor_type=${pieces[0]}
    local partition_layout=${partition_layout_map[${pieces[1]}]}
    local gapps_selection=${gapps_selection_map[${pieces[2]}]}
    local su_selection=${su_selection_map[${pieces[3]}]}
    local build_type_selection=${pieces[4]}

    if [[ -z "$processor_type" || -z "$partition_layout" || -z "$gapps_selection" || -z "$su_selection" ]]; then
        >&2 echo "Invalid variant '$1'"
        >&2 help
        exit 2
    fi

    echo "treble_${processor_type}_${partition_layout}${gapps_selection}${su_selection}-${build_type_selection}"
}

declare -a variant_codes
declare -a variant_names
function get_variants() {
    while [[ $# -gt 0 ]]; do
        case "$1" in
            *-*-*-*-*)
                variant_codes[${#variant_codes[*]}]=$(parse_variant "$1")
                variant_names[${#variant_names[*]}]="$1"
                ;;
            *-*-*-*)
                variant_codes[${#variant_codes[*]}]=$(parse_variant "$1-userdebug")
                variant_names[${#variant_names[*]}]="$1"
                ;;
        esac
        shift
    done
}

## function that actually do things

function init_release() {
    mkdir -p release/"$rom_fp"
}

function init_main_repo() {
    repo init -u "$mainrepo" -b "$mainbranch"
}

function clone_or_checkout() {
    local dir="$1"
    local repo="$2"

    if [[ -d "$dir" ]];then
        (
            cd "$dir"
            git fetch
            git reset --hard
            git checkout origin/"$localManifestBranch"
        )
    else
        git clone https://gitlab.com/ExpressLukeTreble/"$repo" "$dir" -b "$localManifestBranch"
    fi
}

function init_local_manifest() {
    clone_or_checkout .repo/local_manifests treble_manifest
}

function init_patches() {
    if [[ -n "$treble_generate" ]]; then
        clone_or_checkout patches treble_patches

        # We don't want to replace from AOSP since we'll be applying
        # patches by hand
        rm -f .repo/local_manifests/replace.xml

        # Remove exfat entry from local_manifest if it exists in ROM manifest 
        if grep -rqF exfat .repo/manifests || grep -qF exfat .repo/manifest.xml;then
            sed -i -E '/external\/exfat/d' .repo/local_manifests/manifest.xml
        fi
    fi
}

function sync_repo() {
    if [[ $nogapps == "true" ]]; then
      echo Removing GApps manifest
      rm -rf .repo/local_manifests/pe_gapps.xml
    fi
    repo sync -c -j "$jobs" -f --force-sync --no-tag --no-clone-bundle --optimized-fetch --prune
}

function fix_missings() {
	if [[ "$localManifestBranch" == *"10"* ]]; then
	        rm -rf vendor/*/packages/overlays/NoCutout*
		# fix kernel source missing (on Q)
		sed 's;.*KERNEL_;//&;' -i vendor/*/build/soong/Android.bp 2>/dev/null || true
		mkdir -p device/sample/etc
		cd device/sample/etc
		wget -O apns-full-conf.xml https://github.com/LineageOS/android_vendor_lineage/raw/lineage-17.1/prebuilt/common/etc/apns-conf.xml 2>/dev/null
		cd ../../..
		mkdir -p device/generic/common/nfc
		cd device/generic/common/nfc
		wget -O libnfc-nci.conf https://github.com/ExpressLuke/treble_experimentations/raw/master/files/libnfc-nci.conf
		cd ../../../..
		sed -i '/Copies the APN/,/include $(BUILD_PREBUILT)/{/include $(BUILD_PREBUILT)/ s/.*/ /; t; d}' vendor/*/prebuilt/common/Android.mk 2>/dev/null || true
	fi
}

function patch_things() {
    if [[ -n "$treble_generate" ]]; then
        rm -f device/*/sepolicy/common/private/genfs_contexts
        (
            cd device/phh/treble
    if [[ $choice == *"y"* ]];then
            git clean -fdx
    fi
            bash generate.sh "$treble_generate"
        )
        bash "$(dirname "$0")/apply-patches.sh" patches
    else
        (
            cd device/phh/treble
            git clean -fdx
            bash generate.sh
        )
        repo manifest -r > release/"$rom_fp"/manifest.xml
        bash "$(dirname "$0")"/list-patches.sh
        cp patches.zip release/"$rom_fp"/patches.zip
    fi
}

function rom_patches() {
    if [[ $romname == *havoc* ]]; then
        cd frameworks/base
        wget https://gitlab.com/expressluke-treble/rom_patches/raw/master/havoc/0001-Fix-Samsung-MTK-RadioIndication-RadioResponse.patch
        wget https://gitlab.com/expressluke-treble/rom_patches/raw/master/havoc/0002-Remove-Vendor-Mismatch-Warning.patch
        wget https://gitlab.com/expressluke-treble/rom_patches/raw/master/havoc/0003-Include-Vendor-Samsung-Hardware-Light-2.0.patch
        wget https://gitlab.com/expressluke-treble/rom_patches/raw/master/havoc/0001-Havoc-WIP.patch
        wget https://gitlab.com/expressluke-treble/rom_patches/raw/master/havoc/0002-Forawrdport-Samsung-fod-support-for-ultrasound-fp.patch
        wget https://gitlab.com/expressluke-treble/rom_patches/raw/master/havoc/0003-Improve-FacolaView-life-cycle.patch
        wget https://gitlab.com/expressluke-treble/rom_patches/raw/master/havoc/0004-Add-a-nodim-property-for-FacolaView.patch
        wget https://gitlab.com/expressluke-treble/rom_patches/raw/master/havoc/0005-FacolaView-Support-goodix-ext-and-fix-wrong-forced.patch
        wget https://gitlab.com/expressluke-treble/rom_patches/raw/master/havoc/0006-Havoc-WIP-2.patch
        wget https://gitlab.com/expressluke-treble/rom_patches/raw/master/havoc/0007-misc-havoc-mess.patch
        git am 0001-Fix-Samsung-MTK-RadioIndication-RadioResponse.patch
        git am 0002-Remove-Vendor-Mismatch-Warning.patch
        git am 0003-Include-Vendor-Samsung-Hardware-Light-2.0.patch
        git am 0001-Havoc-WIP.patch
        git am 0002-Forawrdport-Samsung-fod-support-for-ultrasound-fp.patch
        git am 0003-Improve-FacolaView-life-cycle.patch 
        git am 0004-Add-a-nodim-property-for-FacolaView.patch
        git am 0005-FacolaView-Support-goodix-ext-and-fix-wrong-forced.patch
        git am 0006-Havoc-WIP-2.patch
        git am 0007-misc-havoc-mess.patch
        cd ../..
    fi
    if [[ $romname == *evolutionx* ]]; then
        rm -rf vendor/google/customization/interfaces/wifi_ext/libs/Android.bp
        cd frameworks/base
        wget https://gitlab.com/expressluke-treble/rom_patches/raw/master/evolutionx/0001-Fix-Samsung-MTK-RadioIndication-RadioResponse.patch
        git am 0001-Fix-Samsung-MTK-RadioIndication-RadioResponse.patch
        cd ../..
        cd frameworks/av
        git revert 00f750d --no-edit
        git revert 1181d39 --no-edit
        cd ../..
    fi
    if [[ $romname == *pixelexperience* ]]; then
        rm -rf vendor/google/customization/interfaces/wifi_ext/libs/Android.bp
        cd frameworks/base
        echo "I'm in frameworks/base!"
        wget https://gitlab.com/expressluke-treble/rom_patches/raw/master/pixelexperience/0001-Import-SystemProperties.patch
        wget https://gitlab.com/expressluke-treble/rom_patches/raw/master/pixelexperience/0002-Fix-Samsung-MTK-RadioIndication-RadioResponse.patch
        wget https://gitlab.com/expressluke-treble/rom_patches/raw/master/pixelexperience/0001-PixelExperience-WIP.patch
        wget https://gitlab.com/expressluke-treble/rom_patches/raw/master/pixelexperience/0002-Forawrdport-Samsung-fod-support-for-ultrasound-fp.patch
        wget https://gitlab.com/expressluke-treble/rom_patches/raw/master/pixelexperience/0003-Improve-FacolaView-life-cycle.patch
        wget https://gitlab.com/expressluke-treble/rom_patches/raw/master/pixelexperience/0004-Add-a-nodim-property-for-FacolaView.patch
        wget https://gitlab.com/expressluke-treble/rom_patches/raw/master/pixelexperience/pixelexperience/0005-FacolaView-Support-goodix-ext-and-fix-wrong-forced-b.patch
        git am 0001-Import-SystemProperties.patch
        git am 0002-Fix-Samsung-MTK-RadioIndication-RadioResponse.patch
        git am 0001-PixelExperience-WIP.patch
        git am 0002-Forawrdport-Samsung-fod-support-for-ultrasound-fp.patch
        git am 0003-Improve-FacolaView-life-cycle.patch
        git am 0004-Add-a-nodim-property-for-FacolaView.patch
        git am 0005-FacolaView-Support-goodix-ext-and-fix-wrong-forced-b.patch
        git revert 04b4144dc1a5654f19cc426d3c971f1315abe711 --no-edit
        cd ../..
        cd frameworks/av
        echo "I'm in frameworks/av!"
        git revert 00f750d --no-edit
        git revert 1181d39 --no-edit
        cd ../..
    fi
    if [[ $romname == *potato* ]]; then
        cd frameworks/base
        echo "I'm in frameworks/base!"
        wget https://gitlab.com/expressluke-treble/rom_patches/raw/master/potato/0001-Fix-Samsung-MTK-RadioIndication-RadioResponse.patch
        wget https://gitlab.com/expressluke-treble/rom_patches/raw/master/potato/0002-Potato-WIP.patch
        wget https://gitlab.com/expressluke-treble/rom_patches/raw/master/potato/0003-Forawrdport-Samsung-fod-support-for-ultrasound-fp.patch
        wget https://gitlab.com/expressluke-treble/rom_patches/raw/master/potato/0004-Improve-FacolaView-life-cycle.patch
        wget https://gitlab.com/expressluke-treble/rom_patches/raw/master/potato/0005-Add-a-nodim-property-for-FacolaView.patch
        wget https://gitlab.com/expressluke-treble/rom_patches/raw/master/potato/0006-FacolaView-Support-goodix-ext-and-fix-wrong-forced-b.patch
        git am 0001-Fix-Samsung-MTK-RadioIndication-RadioResponse.patch
        git am 0002-Potato-WIP.patch
        git am 0003-Forawrdport-Samsung-fod-support-for-ultrasound-fp.patch
        git am 0004-Improve-FacolaView-life-cycle.patch
        git am 0005-Add-a-nodim-property-for-FacolaView.patch
        git am 0006-FacolaView-Support-goodix-ext-and-fix-wrong-forced-b.patch
        git revert a589725c36e4ec0023d405c8ae8912dda530ed10 --no-edit
        cd ../..
     fi
    if [[ $romname == *extendedui* ]]; then
        echo "This is extendedUI rom, stuff to appear soon."
        rm -rf vendor/google/customization/interfaces/wifi_ext/libs/Android.bp
        cd frameworks/av
        git revert b88ddff43770db7874f4f8b557f7a9f0edcafe03 --no-edit
        git revert 88151774273c731d0f64650a329eae1944ee38e8 --no-edit
        cd ../..
        cd frameworks/base
        wget https://gitlab.com/expressluke-treble/rom_patches/raw/master/extendedui/0001-Import-SystemProperties.patch
        git am 0001-Import-SystemProperties.patch
        wget https://gitlab.com/expressluke-treble/rom_patches/raw/master/extendedui/0002-Fix-Samsung-MTK-RadioIndication-RadioResponse.patch
        git am 0002-Fix-Samsung-MTK-RadioIndication-RadioResponse.patch
        wget https://gitlab.com/expressluke-treble/rom_patches/raw/master/extendedui/0003-Remove-vendor-mismatch-warning.patch
        git am 0003-Remove-vendor-mismatch-warning.patch
        cd ../..
    fi
}

function build_variant() {
    lunch "$1"
    make $extra_make_options BUILD_NUMBER="$rom_fp" installclean
    make $extra_make_options BUILD_NUMBER="$rom_fp" -j "$jobs" systemimage
    make $extra_make_options BUILD_NUMBER="$rom_fp" vndk-test-sepolicy
    cp "$OUT"/system.img release/"$rom_fp"/system-"$2".img
}

function jack_env() {
    RAM=$(free | awk '/^Mem:/{ printf("%0.f", $2/(1024^2))}') #calculating how much RAM (wow, such ram)
    if [[ "$RAM" -lt 16 ]];then #if we're poor guys with less than 16gb
	export JACK_SERVER_VM_ARGUMENTS="-Dfile.encoding=UTF-8 -XX:+TieredCompilation -Xmx"$((RAM -1))"G"
    fi
}

function clean_build() {
    make installclean
    rm -rf "$OUT"
}

parse_options "$@"
get_rom_type "$@"
get_variants "$@"

if [[ -z "$mainrepo" || ${#variant_codes[*]} -eq 0 ]]; then
    >&2 help
    exit 1
fi

# Use a python2 virtualenv if system python is python3
python=$(python -V | awk '{print $2}' | head -c2)
if [[ $python == "3." ]]; then
    if [ ! -d .venv ]; then
        virtualenv2 .venv
    fi
    . .venv/bin/activate
fi

init_release
if [[ $choice == *"y"* ]];then
    init_main_repo
    init_local_manifest
    init_patches
    sync_repo
fi

patch_things
rom_patches
fix_missings

if [[ $jack_enabled == "true" ]]; then
    jack_env
fi

read -p "Do you want to clean? (y/N) " clean

if [[ $clean == *"y"* ]];then
    clean_build
fi

. build/envsetup.sh

for (( idx=0; idx < ${#variant_codes[*]}; idx++ )); do
    build_variant "${variant_codes[$idx]}" "${variant_names[$idx]}"
done
